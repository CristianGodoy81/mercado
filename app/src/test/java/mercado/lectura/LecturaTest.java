package mercado.lectura;

import mercado.Lectura.Lectura;
import static org.junit.Assert.*;
import org.junit.Test;
import java.time.LocalDate;

public class LecturaTest{

    //compruebo que se agrega crea correctamente una lectura
    @Test
    public void comprobarLecturatest(){
        
        Lectura lecturacomprobar = new Lectura(LocalDate.of(2024,05,21),5700.00);
        
     
        assertEquals(LocalDate.parse("2024-05-21"),lecturacomprobar.getFecha());
    }
}
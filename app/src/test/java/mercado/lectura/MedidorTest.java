package mercado.lectura;

import static org.junit.Assert.*;
import org.junit.Test;

import mercado.Lectura.Lectura;
import mercado.Lectura.LecturaExistenteException;
import mercado.Lectura.LecturaNoExistenteException;
import mercado.Lectura.Medidor;

import java.time.LocalDate;

public class MedidorTest{

    //compruebo crear medidor
    @Test 
     public void comprobarMedidor()throws LecturaExistenteException{
      Medidor medidor = new Medidor("BB68",5900,"XJJm2");
    
       assertEquals(0, medidor.getLecturas().size());
    }
   
    //compruebo registrar lectura nueva
    @Test
    public void testRegistrarLectura()throws LecturaExistenteException{
        Lectura lectura1 = new Lectura(LocalDate.parse("2024-05-20"),5000.00);
        Medidor medidoruno = new Medidor("A468",5460,"Xmp12");
         medidoruno.addLectura(lectura1);
        assertEquals(1,medidoruno.getLecturas().size());

    }

    //registro lectura existente, lanza una excepcion LecturaExistenteException
     @Test  (expected = LecturaExistenteException.class)

    public void testRegistrarLecturaExistente()throws LecturaExistenteException{
        Lectura lectura1 = new Lectura(LocalDate.parse("2024-05-20"),5000.00);
        Medidor medidoruno = new Medidor("A468",5460,"Xmp12");
         medidoruno.addLectura(lectura1);
         medidoruno.addLectura(lectura1);

    }

    //elimino lectura existente del medidor
    @Test
    public void eliminarLecturaTest()throws LecturaNoExistenteException,LecturaExistenteException{

        Lectura lecturaeliminar = new Lectura (LocalDate.parse("2024-04-20"),700);
        Medidor medidoruno = new Medidor("ABC",1060,"Medidor12");

        medidoruno.addLectura(lecturaeliminar);
        medidoruno.eliminaLectura(lecturaeliminar.getFecha());

        assertEquals(0,medidoruno.getLecturas().size());

        }

    //elimina lectura no existente, lanza la excepcion LecturaNoExistenteException
    @Test (expected = LecturaNoExistenteException.class)

    public void testEliminarLecturaNoExistente()throws LecturaNoExistenteException,LecturaExistenteException{

        Lectura lecturaeliminar = new Lectura (LocalDate.parse("2024-04-20"),700);
        Medidor medidoruno = new Medidor("ABC",1060,"Medidor12");

        medidoruno.addLectura(lecturaeliminar);
        medidoruno.eliminaLectura(LocalDate.of(2024,05,21));

        }

    //modifico lectura existente en medidor
    @Test
    public void modificarLecturatest()throws LecturaNoExistenteException,LecturaExistenteException{

        Lectura lecturamodificar = new Lectura (LocalDate.parse("2024-05-20"),900);
        Medidor medidor = new Medidor("BB100",5900,"Medidor123");
        medidor.addLectura(lecturamodificar);  

        Lectura lecturanueva = new Lectura (LocalDate.parse("2024-05-20"),1000);
        medidor.modificarLectura(lecturanueva);
        Lectura lectura = medidor.getLectura(lecturanueva.getFecha());
        assertEquals(lecturanueva.getMontomensual(),lectura.getMontomensual());
    }

    //modifico lectura no existente, lanza una exception LecturaNoExistenteException
    @Test (expected = LecturaNoExistenteException.class)

    public void modificarLecturaNoExistentetest()throws LecturaNoExistenteException,LecturaExistenteException{

        Lectura lecturamodificar = new Lectura (LocalDate.parse("2024-05-21"),1200);
        Medidor medidor = new Medidor("AA00",5900,"Medidor120");
        medidor.addLectura(lecturamodificar);  

        Lectura lecturanueva = new Lectura (LocalDate.parse("2024-06-20"),1000);
        medidor.modificarLectura(lecturanueva);

    }

    //busco lectura existente en el medidor
    @Test 
    public void testBuscarLectura()throws LecturaNoExistenteException{
        Lectura lecturabuscar = new Lectura (LocalDate.of(2024,04,25),1200);
        Medidor medidor = new Medidor("32",1700,"Medidor001");
        medidor.addLectura(lecturabuscar); 

        boolean iguales= medidor.getLectura(LocalDate.parse("2024-04-25")).equals(lecturabuscar);

        assertEquals(true,iguales);
    }

    //busco lectura no existente en el medidor, lanzo excepcion LecturaNoExistente
    @Test  (expected = LecturaNoExistenteException.class)

    public void buscarLecturaNoExistente() throws LecturaNoExistenteException,LecturaExistenteException {
        Medidor medidor = new Medidor("60",1700,"Medidor003");
        Lectura lecturabuscar = new Lectura (LocalDate.of(2024,04,23),1350);
        
        medidor.addLectura(lecturabuscar);

        medidor.buscarLectura(LocalDate.of(2024,05,24));
    }
}


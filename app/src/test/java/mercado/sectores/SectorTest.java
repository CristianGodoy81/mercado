package mercado.sectores;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SectorTest {
    @Test
    public void equalsTest(){
        Sector sectorUno = new Sector("Jugeteria","Norte");
        Sector sectorDos = new Sector("libreria","Norte");

        boolean x = sectorUno.equals(sectorDos);

        assertEquals(false, x);
    }
}

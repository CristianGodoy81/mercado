package mercado.sectores;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import mercado.Lectura.Medidor;

public class PuestoTest {
    @Test
    public void equalsTest(){
        Medidor medidorUno = new Medidor("001", 9999, "Pickachu");
        Medidor medidorDos = new Medidor("002", 8888, "Pickachu");

        Puesto puestoUno = new Puesto(23, false, false, 10, 15000, true, medidorUno);
        Puesto puestoDos = new Puesto(23, false, false, 10, 15000, true, medidorDos);

        boolean x = puestoUno.equals(puestoDos);

        assertEquals(true, x);
    }
}

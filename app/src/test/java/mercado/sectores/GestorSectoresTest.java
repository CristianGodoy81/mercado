package mercado.sectores;
import static org.junit.Assert.*;
import org.junit.Test;
import mercado.Lectura.*;

public class GestorSectoresTest {

    //compruebo creacion de Gestor de Sectores
    @Test 
    public void comprobarCrearGestorSectores()throws SectorNoExistenteException{

        GestorSectores sectores = new GestorSectores();

        assertEquals(0,sectores.getSectores().size());
    }


    //creo un sector no existente
    @Test
    public void comprobarAgregarSectorTest()throws SectorExistenteException{
        GestorSectores sectores = new GestorSectores();
        Medidor medidor = new Medidor("23",600,"Medidor56");

        Puesto puesto = new Puesto(9,true,false,23,4000,true,medidor);
   
        Sector sectorcomida = new Sector("Comida","este");

        sectorcomida.agregarPuesto(puesto);

        sectores.agregarSector(sectorcomida);

        assertEquals(1,sectores.getSectores().size());
    }

    //compruebo agregar un sector existente, lanzo la excepcion Sector existente
    @Test  (expected = SectorExistenteException.class)

    public void comprueboCrearSectorExistenteTest()throws SectorExistenteException{

        GestorSectores sectores = new GestorSectores();
        Medidor medidor = new Medidor("20",690,"Medidor11");

        Puesto puesto = new Puesto(1,false,true,25,7000,true,medidor);
   
        Sector sectorlibreria = new Sector("Libreria","norte");

        sectorlibreria.agregarPuesto(puesto);

        sectores.agregarSector(sectorlibreria);

        sectores.agregarSector(sectorlibreria);
    
    }

    //busco Sector existente
    @Test

    public void buscoSectorExistenteTest()throws SectorExistenteException,SectorNoExistenteException{

        GestorSectores sectores = new GestorSectores();
        Medidor medidor = new Medidor("20",1200,"Medidor12");

        Puesto puesto = new Puesto(7,true,false,20,3000,true,medidor);
   
        Sector sectorjugueteria = new Sector("Jugueteria","sur");

        sectorjugueteria.agregarPuesto(puesto);

        sectores.agregarSector(sectorjugueteria);

        assertEquals("Jugueteria",(sectores.getSector(sectorjugueteria)).getNombre());
    }


    //busco sector no existente, lanzo excepcion
    @Test (expected = SectorNoExistenteException.class)

    public void buscoSectorNoExistenteTest()throws SectorExistenteException,SectorNoExistenteException{

        GestorSectores sectores = new GestorSectores();
        Medidor medidor = new Medidor("20",1200,"Medidor12");

        Puesto puesto = new Puesto(2,true,false,20,3000,true,medidor);
   
        Sector sectormueble = new Sector("Mueble","sur");

        sectormueble.agregarPuesto(puesto);

        sectores.getSector(sectormueble);

    }

    //elimino sector existente
    @Test
    public void eliminoSectorTest()throws SectorExistenteException,SectorNoExistenteException{

        GestorSectores sectores = new GestorSectores();
        Medidor medidor = new Medidor("25",1200,"Medidor13");

        Puesto puesto = new Puesto(3,true,false,20,3000,true,medidor);
   
        Sector sectorcomida = new Sector("Comida","norte");

        sectorcomida.agregarPuesto(puesto);

        sectores.agregarSector(sectorcomida);
        sectores.quitarSector(sectorcomida);

        assertEquals(0,sectores.getSectores().size());
    }

    //elimino sector no existente, lanzo excepcion
    @Test (expected= SectorNoExistenteException.class)
    public void eliminoSectorNoExistenteTest()throws SectorExistenteException,SectorExistenteException{
        GestorSectores sectores = new GestorSectores();
        Medidor medidor = new Medidor("25",1200,"Medidor13");

        Puesto puesto = new Puesto(4,true,false,20,3000,true,medidor);
   
        Sector sectorcomida = new Sector("Comida","norte");

        sectorcomida.agregarPuesto(puesto);

        sectores.quitarSector(sectorcomida);
    }

    //modifico un sector existente
    @Test
    public void modificoSectorTest()throws SectorExistenteException,SectorNoExistenteException{
        GestorSectores sectores = new GestorSectores();

        Medidor medidor = new Medidor("26",1300,"Medidor14");

        Puesto puesto = new Puesto(5,true,true,21,5000,true,medidor);
   
        Sector sectorverduleria = new Sector("Verduleria","oeste");

        sectorverduleria.agregarPuesto(puesto); 
        sectores.agregarSector(sectorverduleria);

        Sector sectorverduleriamodificar = new Sector("Verduleria","este");
        sectores.modificarSector(sectorverduleriamodificar);

        assertEquals("este",(sectores.getSector(sectorverduleriamodificar)).getUbicacion());
        
    }

    //modifico un sector no existente, lanzo una excepcion Sector no Existente
    @Test (expected = SectorNoExistenteException.class)

    public void modificoSectorNoExistenteTest()throws SectorExistenteException,SectorNoExistenteException{

        GestorSectores sectores = new GestorSectores();
        Medidor medidor = new Medidor("27",1500,"Medidor15");

        Puesto puesto = new Puesto(10,true,true,21,9000,true,medidor);
   
        Sector sectorverduleria = new Sector("Verduleria","oeste");

        sectorverduleria.agregarPuesto(puesto);

        Sector sectorverduleriamodificar = new Sector("Verduleria","este");
        sectores.modificarSector(sectorverduleriamodificar);

    }

}

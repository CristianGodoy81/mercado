package mercado.clientes;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ClienteTest {
    @Test
    public void equalsTest(){
        Quintero clienteUno = new Quintero("111", "154111111", "mail1@mail.com", "Calle 111", "Juan", "Perez");
        Quintero clienteDos = new Quintero("111", "154222222", "mail2@mail.com", "Calle 222", "Luis", "Perez");
        //Quintero clienteDos = new Quintero("222", "154333333", "mail3@mail.com", "otraCalle 333", "Pedro", "Perez");

        boolean x = clienteUno.equals(clienteDos);

        assertEquals(true, x);
    }
}

package mercado.clientes;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

public class RegistroClientesTest {

    @Test
    public void testComprobarCreacionRegistroCliente() throws ClienteExistenteException{
        RegistroClientes clientes = new RegistroClientes();

        assertEquals(0,clientes.getClientes().size());

    }
    
    // registro cliente nuevo
    @Test
    public void testRegistrarCliente() throws ClienteExistenteException{

        Cliente cliente = new Quintero ("103450", "154111111", "RRDiaz111@mail.com", "Calle 111", "Raul", "Diaz");
    
        RegistroClientes clientes = new RegistroClientes();

        clientes.agregarCliente(cliente);
        assertEquals(1,clientes.getClientes().size());
    }

    //registro cliente existente, lanzo excepcion ClienteNoExistente
    @Test (expected = ClienteExistenteException.class)

    public void testRegistrarClienteExistente() throws ClienteExistenteException{

        Cliente cliente = new Quintero ("129850", "383456091", "RRDiaz111@mail.com", "Calle 111", "Raul", "Diaz");
    
        RegistroClientes clientes = new RegistroClientes();

        clientes.agregarCliente(cliente);
        clientes.agregarCliente(cliente);

    }

    //Busco cliente existente
    @Test 
    public void buscoClienteTest()throws ClienteExistenteException,ClienteNoExistenteException{
        Cliente cliente = new Empresa ("123000", "383455990", "LibreriaMaestro@gmail.com","calle 500","Libreria Maestro");
        RegistroClientes clientes = new RegistroClientes();

        clientes.agregarCliente(cliente);

        assertEquals("123000",(clientes.buscarCliente(cliente)).getCuit());
    }


    //Busco cliente no existente, lanzo una excepcion Cliente no encontrado
    @Test (expected = ClienteNoExistenteException.class)

    public void comprueboBuscarClienteNoExistenteTest() throws ClienteNoExistenteException, ClienteExistenteException{

        Cliente cliente = new Empresa("123000", "423300", "LibreriaM@gmail.com","calle450","Libreria Maestro");

        RegistroClientes clientes = new RegistroClientes();
        clientes.agregarCliente(cliente);

        Cliente clientebuscar = new Empresa ("444000", "383400001", "correoinexistente@gmail.com","calle 501","FIambreria Lopezz");
        clientes.buscarCliente(clientebuscar);
 
    }

    //modifico cliente existente
    @Test 
    public void comprueboModificaClienteTest ()throws ClienteExistenteException,ClienteNoExistenteException{

        Cliente cliente = new Empresa("133000", "426300", "pensamiento23@gmail.com","calle457","Fiambreria Catamarca");

        RegistroClientes clientes = new RegistroClientes();
        clientes.agregarCliente(cliente);

        Cliente clientemodificar = new Empresa("133000", "427900", "L124@gmail.com","calle460","Fiambreria Catamarca");

        clientes.modificarCliente(clientemodificar);
    }

    //modifico cliente no existente, lanzo una excepcion Cliente No existente
    @Test  (expected = ClienteNoExistenteException.class)

    public void modificoClienteNoExistenteTest()throws ClienteExistenteException,ClienteNoExistenteException{
        Cliente cliente = new Empresa("180050", "427310", "correoinexistente23@gmail.com","Maipu 560","Rosticeria loNuestro");

        RegistroClientes clientes = new RegistroClientes();
        clientes.agregarCliente(cliente);

        Cliente clientemodificar = new Empresa("133000", "427900", "L124@gmail.com","calle460","Fiambreria Catamarca");

        clientes.modificarCliente(clientemodificar);
    }

    //elimino cliente existente
    @Test
    public void eliminoClienteTest()throws ClienteExistenteException, ClienteNoExistenteException{

        Cliente cliente = new Empresa("199050", "421010", "limpieza23@gmail.com","Zurita 960","Hogar Limpieza");

        RegistroClientes clientes = new RegistroClientes();
        
        clientes.agregarCliente(cliente);

        clientes.quitarCliente(cliente);
        assertEquals(0,clientes.getClientes().size());
    }

    //elimino cliente no existente, lanzo excepcion Cliente no existente
    @Test  (expected = ClienteNoExistenteException.class)

    public void comprueboEliminarClienteNoExistenteTest() throws ClienteNoExistenteException,ClienteExistenteException{
        Cliente cliente = new Empresa("377050", "420700", "comidarapida123@gmail.com","Almagro 960","Comedor Almagro");

        RegistroClientes clientes = new RegistroClientes();
        
        clientes.agregarCliente(cliente);
        Cliente clientenoregistrado = new Empresa("18850", "420700", "comidarapida123@gmail.com","Almagro 960","Comedor Almagro");

        clientes.quitarCliente(clientenoregistrado);

    }
}

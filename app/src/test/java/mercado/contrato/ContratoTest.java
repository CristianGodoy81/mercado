package mercado.contrato;

import java.time.LocalDate;

import mercado.clientes.*;
import mercado.Lectura.Lectura;
import mercado.Lectura.Medidor;
import mercado.sectores.Puesto;
import mercado.Contrato.Contrato;
import mercado.Contrato.ContratoExistenteException;
import mercado.Lectura.LecturaExistenteException;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class ContratoTest { 

    @Test
    public void comprobarCreaciondeContratoTest()throws ContratoExistenteException,LecturaExistenteException{

    Lectura lectura = new Lectura (LocalDate.parse("2024-04-21"),4000);
    Cliente cliente = new Quintero("1309870","435002","cliente1@gmail.com","Av. Belgrano 667","Juan","Gracia");
    Medidor medidoruno = new Medidor("48",540,"Medidor12");
    medidoruno.addLectura(lectura);
    Puesto puesto1 = new Puesto(23, false, false, 10, 15000, true, medidoruno);
    Contrato contratonuevo = new Contrato(3201,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
   
    Medidor medidordos = new Medidor("50",700,"MedidorWM");
    Lectura lecturados = new Lectura(LocalDate.of(2024,05,21),800);
    medidordos.addLectura(lecturados);
    Puesto puesto2= new Puesto(230,false,true,4.2,900,true,medidordos); 
    Contrato contratocomprobar = new Contrato(2000,LocalDate.of(2024,03,12),LocalDate.of(2024,8,12),lecturados,6500,cliente,"Jose Gimenez",puesto2); 

    boolean comprobar= contratocomprobar.equals(contratonuevo);

    assertEquals(false,comprobar);
    }
}

package mercado.contrato;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.time.LocalDate;

import mercado.Contrato.Contrato;
import mercado.Contrato.ContratoExistenteException;
import mercado.Contrato.ContratoNoExistenteException;
import mercado.Contrato.FicherosdeContratos;
import mercado.Lectura.Lectura;
import mercado.Lectura.LecturaExistenteException;
import mercado.Lectura.Medidor;
import mercado.clientes.Cliente;
import mercado.clientes.Quintero;
import mercado.sectores.Puesto;

public class FicherosdeContratoTest {
    
    //compruebo crear ficheroContrato
    @Test
    public void comprobarFicheroContrato(){
        FicherosdeContratos ficheroprueba = new FicherosdeContratos();

        assertEquals(0,ficheroprueba.getContratos().size());
    }

    //compruebo agregar un contrato nuevo a un fichero 
    @Test
    public void comprobarAgregarContratoenFicheroContrato() throws ContratoExistenteException,LecturaExistenteException{

        FicherosdeContratos fichero1 = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.parse("2024-04-21"),4000);
        Cliente cliente = new Quintero("2209870","435002","juan123@gmail.com","maipu 667","Juan","Gracia");
        Medidor medidora = new Medidor("100",540,"Medidor12");
        medidora.addLectura(lectura);
        Puesto puesto1 = new Puesto(23, false, false, 10, 15000, true, medidora);
        Contrato contratoa = new Contrato(3201,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
        fichero1.agregarContrato(contratoa);

        assertEquals(1,fichero1.getContratos().size());
    }

    //compruebo agregar un contrato ya existente y lanzar la excepcion ContratoExistenteException
    @Test (expected = ContratoExistenteException.class)
    public void comprobarAgregarContratoExisteneFicheroContrato() throws ContratoExistenteException,LecturaExistenteException{

        FicherosdeContratos fichero1 = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.parse("2024-04-21"),4000);
        Cliente cliente = new Quintero("2209870","435002","juan123@gmail.com","maipu 667","Juan","Gracia");
        Medidor medidora = new Medidor("100",540,"Medidor12");
        medidora.addLectura(lectura);
        Puesto puesto1 = new Puesto(23, false, false, 10, 15000, true, medidora);
        Contrato contratoa = new Contrato(3201,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
        fichero1.agregarContrato(contratoa);

        fichero1.agregarContrato(contratoa);

    }
    
    //compruebo eliminar un contrato existente en el fichero
    @Test
    public void comprobarEliminarContratodeFicheroContrato()throws ContratoNoExistenteException {

        FicherosdeContratos carpetacontrato = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.parse("2024-04-21"),4000);
        Cliente cliente = new Quintero("2209870","435002","juan123@gmail.com","maipu 667","Juan","Gracia");
        Medidor medidora = new Medidor("100",540,"Medidor12");
        medidora.addLectura(lectura);
        Puesto puesto1 = new Puesto(23, true, true, 10, 19000, true, medidora);
        Contrato contratoa = new Contrato(3201,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
        carpetacontrato.agregarContrato(contratoa);

        Lectura otralectura = new Lectura (LocalDate.parse("2024-04-21"),600);
        Cliente otrocliente = new Quintero("1109870","435077","MMati123@gmail.com","Av. Virgen del Valle 667","Matias","Marquez");
        Medidor medidordos = new Medidor("101",540,"Medidor13");
        medidordos.addLectura(lectura);
        Puesto puestito = new Puesto(20, true, false, 10, 15000, false, medidordos);
        Contrato contratodos = new Contrato(3055,LocalDate.of(2024,06,04),LocalDate.of(2024,12,12),otralectura,7000,otrocliente,"Jose Gimenez",puestito);
        carpetacontrato.agregarContrato(contratodos);

        carpetacontrato.eliminarContrato(3201);

        assertEquals(1,carpetacontrato.getContratos().size());
    }

    //compruebo lanzar la excepcion ContratoNoExistente para el metodo eliminar contrato
    @Test  (expected = ContratoNoExistenteException.class)

    public void eliminarContratoNoExistenteTest()throws ContratoNoExistenteException{
        FicherosdeContratos carpetacontrato = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.of(2024,3,21),2500);
        Cliente cliente = new Quintero("1709870","400102","Emmbustos123@gmail.com","Av. Belgrano 440","Emanuel","Bustos");
        Medidor medidora = new Medidor("36",600,"Medidor123");
        medidora.addLectura(lectura);
        Puesto puesto1 = new Puesto(12, true, false, 10, 13000, true, medidora);
        Contrato contratoa = new Contrato(3001,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
        carpetacontrato.agregarContrato(contratoa);

        carpetacontrato.eliminarContrato(4001);  //mando un numero de contrato no registrado o no existente al metodo eliminarContrato

    }

    //compruebo buscar un contrato existente
    @Test
    public void buscarContratoTest()throws ContratoNoExistenteException{
        FicherosdeContratos carpetacontrato = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.of(2024,05,21),4000);
        Cliente cliente = new Quintero("1709870","400102","Emmbustos123@gmail.com","Av. Belgrano 440","Emanuel","Bustos");
        Medidor medidor = new Medidor("100",600,"Medidor12");
        medidor.addLectura(lectura);
        Puesto puesto1 = new Puesto(12, true, false, 10, 13000, true, medidor);
        Contrato contratoa = new Contrato(3001,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
        carpetacontrato.agregarContrato(contratoa);


        assertEquals(contratoa,carpetacontrato.buscarContrato(contratoa.getNumcontrato()));

    }

    //compruebo buscar contrato no existente y lanzar la excepcion ContratoNoExistente
    @Test (expected =ContratoNoExistenteException.class )

    public void buscarContratoNoExistenteTest()throws ContratoNoExistenteException{
        FicherosdeContratos carpetacontrato = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.parse("2024-05-21"),4000);
        Cliente cliente = new Quintero("1709870","400102","Emmbustos123@gmail.com","Av. Belgrano 440","Emanuel","Bustos");
        Medidor medidora = new Medidor("110",610,"Medidor10");
        medidora.addLectura(lectura);
        Puesto puesto1 = new Puesto(12, true, false, 10, 13000, true, medidora);
        Contrato contratoa = new Contrato(501,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
        carpetacontrato.agregarContrato(contratoa);

        carpetacontrato.buscarContrato(502);

    }

    //compruebo modificar un contrato existente
    @Test

    public void modificarContratoExistenteTest()throws ContratoNoExistenteException{
        FicherosdeContratos carpetacontrato = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.parse("2024-06-21"),1200);
        Cliente cliente = new Quintero("1909870","422100","Rcarlos123@gmail.com","Av. Belgrano 440","Carlos","Rios");
        Medidor medidora = new Medidor("111",610,"Medidor10");
        medidora.addLectura(lectura);
        Puesto puesto1 = new Puesto(13, false, false, 10, 13000, true, medidora);
        Contrato contrato = new Contrato(500,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lectura,6500,cliente,"Jose Gimenez",puesto1);
        carpetacontrato.agregarContrato(contrato);

        
        Lectura lecturanueva = new Lectura (LocalDate.parse("2024-06-21"),1200);
        Medidor medidor = new Medidor("112",750,"Medidor13");
        medidor.addLectura(lecturanueva);
        Puesto puestonuevo = new Puesto(9, true, false, 10, 13000, true, medidor);
        Contrato contratomodificado = new Contrato(500,LocalDate.of(2024,05,04),LocalDate.of(2024,8,12),lecturanueva,6500,cliente,"Jose Gimenez",puestonuevo);
        
        carpetacontrato.modificarContrato(contratomodificado);


        assertEquals(contrato,carpetacontrato.buscarContrato(500));

    }

    //compruebo modificar un contrato no existente, lanzo una excepcion ContratoNoExistente
    @Test  (expected= ContratoNoExistenteException.class)

    public void modificarContratoNoExistenteTest() throws ContratoExistenteException, ContratoExistenteException{
        FicherosdeContratos contratos = new FicherosdeContratos();

        Lectura lectura = new Lectura (LocalDate.parse("2024-06-22"),9100);
        Cliente cliente = new Quintero("1105000","425320","MM123@gmail.com","Av. Belgrano 440","Marcelo","Marquez");
        Medidor medidor = new Medidor("113",110,"Medidor14");
        medidor.addLectura(lectura);
        Puesto puesto1 = new Puesto(15, true, true, 12, 15000, true, medidor);
        Contrato contrato = new Contrato(120,LocalDate.of(2024,06,24),LocalDate.of(2024,12,23),lectura,6500,cliente,"Jose Gimenez",puesto1);
        contratos.agregarContrato(contrato);

        
        Lectura lecturanueva = new Lectura (LocalDate.parse("2024-06-22"),8500);
        Medidor medidornuevo = new Medidor("114",180,"Medidor15");
        medidornuevo.addLectura(lecturanueva);
        Puesto puestonuevo = new Puesto(16, true, false, 12, 15000, true, medidornuevo);
        Contrato contratomodificado = new Contrato(121,LocalDate.of(2024,06,24),LocalDate.of(2024,12,23),lectura,6500,cliente,"Manuel Ocampos",puestonuevo);


        contratos.modificarContrato(contratomodificado);
    }

}

package mercado.Contrato;

public class ContratoNoExistenteException extends RuntimeException {
    public ContratoNoExistenteException(){
        super(" El/los contrato/s no existe/n en el fichero ");
    }
}

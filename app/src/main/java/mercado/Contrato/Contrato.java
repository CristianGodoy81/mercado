package mercado.Contrato;
import java.time.LocalDate;

import mercado.clientes.Cliente;
import mercado.sectores.Puesto;
import mercado.Lectura.Lectura;

public class Contrato{

    private Integer numcontrato;
    private LocalDate fechainicio;
    private LocalDate fechafin;
    private Lectura lectura;
    private double montomensual;
    private Cliente cliente;
    private String arrendatario;
    private Puesto puesto;


    public Contrato(Integer numcontrato,LocalDate fechainicio,LocalDate fechafin,Lectura lectura,double montomensual,Cliente cliente,String arrendatario, Puesto puesto){
    this.numcontrato = numcontrato;
    this.fechainicio = fechainicio;
    this.fechafin=fechafin;
    this.lectura=lectura;
    this.montomensual=montomensual;
    this.cliente=cliente;
    this.arrendatario=arrendatario;
    this.puesto=puesto;

    }
    //get
    public Integer getNumcontrato() {
        return numcontrato;
    }
    public LocalDate getFechainicio() {
        return fechainicio;
    }
    public LocalDate getFechafin() {
        return fechafin;
    }
    public double getMontomensual() {
        return montomensual;
    }
    public String getArrendatario() {
        return arrendatario;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public Lectura getLectura() {
        return lectura;
    }
    public Puesto getPuesto() {
        return puesto;
    }
    //Set
    public void setArrendatario(String arrendatario) {
        this.arrendatario = arrendatario;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public void setFechafin(LocalDate fechafin) {
        this.fechafin = fechafin;
    }
    public void setFechainicio(LocalDate fechainicio) {
        this.fechainicio = fechainicio;
    }
    public void setLectura(Lectura lectura) {
        this.lectura = lectura;
    }
    public void setMontomensual(double montomensual) {
        this.montomensual = montomensual;
    }public void setNumcontrato(Integer numcontrato) {
        this.numcontrato = numcontrato;
    }
    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    @Override
    public boolean equals(Object obj) {
        Contrato aComparar = (Contrato) obj;
        return this.getNumcontrato().equals(aComparar.getNumcontrato());
    }
}

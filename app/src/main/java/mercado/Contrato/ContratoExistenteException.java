package mercado.Contrato;

public class ContratoExistenteException extends RuntimeException{
   public ContratoExistenteException(){
    super(" El contrato ya esta registrado en el Fichero ");
   }
}

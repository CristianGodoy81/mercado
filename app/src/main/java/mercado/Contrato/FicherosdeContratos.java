package mercado.Contrato;

import java.util.ArrayList;
import mercado.clientes.Cliente;

import java.time.LocalDate;

public class FicherosdeContratos {

    private ArrayList <Contrato> contratos;

    //CONSTRUCTOR
    public FicherosdeContratos(){
    setContratos(new ArrayList<Contrato>());
    }

    //get
    public ArrayList<Contrato> getContratos() {
    return contratos;
    }

    //set
    public void setContratos(ArrayList<Contrato> contratos) {
    this.contratos = contratos;
    }


    //agrega un nuevo contrato al array contratos
    public void agregarContrato(Contrato contratonuevo)throws ContratoExistenteException{
  
        if(getContrato(contratonuevo.getNumcontrato())==null){
          this.contratos.add(contratonuevo);
        }else throw new ContratoExistenteException();
       }


    //comprueba la fecha de vencimiento del contrato existente
    public boolean comprobarFechaVencimiento(Contrato contrato, LocalDate fechaactual){
     boolean vencimiento=false;

       if(contrato.getFechafin()==fechaactual) vencimiento =true;
      
      return vencimiento;
    }


    //busca contrato por el numero de contrato
    public Contrato getContrato(Integer numContrato){
     Contrato contratoencontrado=null;

      for (Contrato contrato : contratos){ 
        if(contrato.getNumcontrato().equals(numContrato)){
         contratoencontrado= contrato;               //el contrato existe
        }
      }
        return contratoencontrado;
    }


    //busca un cliente en el array de todos los contratos existentes y devuelve un array con los contratos de ese cliente
    public ArrayList<Contrato> buscarContratosDelCliente(Cliente clientebuscar)throws ContratoNoExistenteException,ContratoExistenteException{
     ArrayList<Contrato> contratosdelcliente= new ArrayList<Contrato>();

      for (Contrato contrato : contratos){
        if(contrato.getCliente().equals(clientebuscar)){
          contratosdelcliente.add(contrato);
        }
      }
        if(contratosdelcliente.size()!= 0){
          return contratosdelcliente; 
        }else throw new ContratoNoExistenteException();

    }


    //elimina un contrato enviandole el numero de contrato como parametro
    public void eliminarContrato(Integer numcontrato)throws ContratoNoExistenteException{
    
        if(getContrato(numcontrato)==null){
          throw new ContratoNoExistenteException();
         }else {
          this.contratos.remove(getContrato(numcontrato));
         }  
    }
    

    //modifica un contrato existente
    public void modificarContrato(Contrato modificacioncontrato)throws ContratoNoExistenteException{
      
        if(getContrato(modificacioncontrato.getNumcontrato())!=null){

        contratos.remove(getContrato(modificacioncontrato.getNumcontrato()));
        contratos.add(modificacioncontrato); 

        }else throw new ContratoNoExistenteException();
    
    }

    //Busco contrato por el numero de contrato y devuelvo el contrato encontrado
    public Contrato buscarContrato(Integer numcontratobuscar){
      
      if(getContrato(numcontratobuscar) == null){
        throw new ContratoNoExistenteException();
      }
      return getContrato(numcontratobuscar);
    }


}




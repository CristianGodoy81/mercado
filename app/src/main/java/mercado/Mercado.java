package mercado;

import mercado.clientes.RegistroClientes;
import mercado.sectores.GestorSectores;
import mercado.Lectura.RegistrodeMedidor;
import mercado.Contrato.*;

public class Mercado {
        private static Mercado instance;
    
    private RegistroClientes clientes = new RegistroClientes();
    private FicherosdeContratos contratos = new FicherosdeContratos();
    private RegistrodeMedidor medidores = new RegistrodeMedidor();
    private GestorSectores sectores = new GestorSectores();
    
    private Mercado(){}
    
    public static Mercado getInstance(){
        if (instance == null) {
            instance = new Mercado();
        }
        return instance;
    }
    
    public RegistroClientes getClientes(){
        return clientes;
    }
    
    public FicherosdeContratos getFicherosdeContrato(){
        return contratos;
    }
    
    public RegistrodeMedidor getMedidores(){
        return medidores;
    }
    
    public GestorSectores getGestorSectores(){
        return sectores;
    }
    
}


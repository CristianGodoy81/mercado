/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mercado.Lectura;

/**
 *
 * @author hp
 */
public class MedidorNoExistenteException extends RuntimeException{
    public MedidorNoExistenteException(){
        super("ERROR.El medidor no esta registrado");
    }
}

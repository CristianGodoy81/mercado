/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mercado.Lectura;

/**
 *
 * @author hp
 */
public class MedidorExistenteException extends RuntimeException{
    public MedidorExistenteException(){
        super("ERROR.El medidor ya esta registrado");
    }
    
}

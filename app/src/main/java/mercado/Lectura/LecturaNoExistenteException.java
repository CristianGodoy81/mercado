package mercado.Lectura;

public class LecturaNoExistenteException extends RuntimeException{
    public LecturaNoExistenteException(){
        super(" La lectura no esta registrada en el Medidor ");
    }
    
}

package mercado.Lectura;

public class LecturaExistenteException extends RuntimeException{
    public LecturaExistenteException(){
        super(" La lectura ya esta registrada en el Medidor ");
    }
}

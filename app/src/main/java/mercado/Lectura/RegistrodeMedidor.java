/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package mercado.Lectura;

import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class RegistrodeMedidor {
    
    private ArrayList <Medidor> medidores;
    
    public RegistrodeMedidor(){
       setMedidores(new ArrayList<Medidor>()); 
    }
    
    public void setMedidores(ArrayList<Medidor> medidores){
        this.medidores = medidores;
    }
    
    
    //agregar medidor nuevo
    public void addMedidor(Medidor nuevomedidor)throws MedidorExistenteException{
        Medidor medidorbuscar = getMedidor(nuevomedidor);
        
        if(medidorbuscar==null){
            medidores.add(nuevomedidor);
        }else{
            throw new MedidorExistenteException();
        }
        
    }
    
    public Medidor getMedidor(Medidor medidorbuscar){
        Medidor medidorencontrado = null;
        
        for(Medidor medidor : medidores){
            if(medidor.equals(medidorbuscar)){
               medidorencontrado=medidor;
               break;
            }
        }
        return medidorencontrado;
    }
    
    public void eliminarMedidor(Medidor medidorremove)throws MedidorNoExistenteException{
        Medidor medidorbuscar=getMedidor(medidorremove);
        
        if(medidorbuscar != null){
            medidores.remove(medidorremove);
        }else{
            throw new MedidorNoExistenteException();
           
        }
    }
    
    public void modificarMedidor(Medidor medidormodificar)throws MedidorNoExistenteException{
        Medidor medidorbuscar = getMedidor(medidormodificar);
        
        if(medidormodificar != null){
            medidores.remove(medidorbuscar);
            medidores.add(medidormodificar);
        }else{
            throw new MedidorNoExistenteException();
            
        }
    }
}

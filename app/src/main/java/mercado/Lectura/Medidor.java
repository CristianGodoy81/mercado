package mercado.Lectura;

import java.util.ArrayList;
import java.time.LocalDate;

public class Medidor {

    private String numserie;
    private float consumoenergia;
    private String marca;
    private ArrayList<Lectura> lecturas= new ArrayList<>();

    public Medidor(String numserie, float consumoenergia, String marca){
        this.numserie=numserie;
        this.consumoenergia=consumoenergia;
        this.marca=marca;
        setLecturas(new ArrayList<Lectura>());
    }
    //gets
    public float getConsumoenergia() {
        return consumoenergia;
    }
    public String getMarca() {
        return marca;
    }
    public String getNumserie() {
        return numserie;
    }
    public ArrayList<Lectura> getLecturas() {
        return lecturas;
    }
    //Sets
    public void setConsumoenergia(float consumoenergia) {
        this.consumoenergia = consumoenergia;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    public void setNumserie(String numserie) {
        this.numserie = numserie;
    }
    public void setLecturas(ArrayList<Lectura> lecturas) {
        this.lecturas = lecturas;
    }

    //Busca una lectura de un medidor por la fecha y devuelve una lectura
    public Lectura getLectura(LocalDate fehcabuscar){
    Lectura lecturaencontrada= null;

        for(Lectura lectura : lecturas){
            if(lectura.getFecha().equals(fehcabuscar))
             lecturaencontrada = lectura;
             break;
        }
        
        return lecturaencontrada;
    }


    //Elimina lectura del medidor

    public void eliminaLectura(LocalDate fechalectura)throws LecturaNoExistenteException{
        Lectura lecturaeliminar = getLectura(fechalectura);

        if(lecturaeliminar==null){
            throw new LecturaNoExistenteException(); 
        }else{
        this.lecturas.remove(lecturaeliminar); 
        }

        }


    //Ingresa lectura nueva al medidor

    public void addLectura(Lectura lecturanueva)throws LecturaExistenteException{
        Lectura lecturaexiste= null;
        lecturaexiste= getLectura(lecturanueva.getFecha());
           
            if(lecturaexiste!= null){
                throw new LecturaExistenteException();
                 }else this.lecturas.add(lecturanueva);
    }
    
    //modifica el monto de una lectura del medidor

    public void modificarLectura(Lectura lecturanueva)throws LecturaNoExistenteException{
       Lectura lecturamodificada= getLectura(lecturanueva.getFecha());

          if(lecturamodificada==null){
            throw  new LecturaNoExistenteException();

          }else{
              eliminaLectura(lecturamodificada.getFecha());
              lecturas.add(lecturanueva);
           }
        }


    //busco lectura, si no existe lanzo una excepcion
    public Lectura buscarLectura(LocalDate fechalecturabuscar)throws LecturaNoExistenteException{

        Lectura lecturaencontrada = getLectura(fechalecturabuscar);
        
        if(lecturaencontrada==null){
            throw new LecturaNoExistenteException();
        }else{
            return lecturaencontrada;
        }

    }
    }


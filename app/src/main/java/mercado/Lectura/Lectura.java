package mercado.Lectura;
import java.time.LocalDate;

public class Lectura {
    private LocalDate fecha;
    private double montomensual;

    public Lectura( LocalDate fecha, double montomensual){
        this.fecha=fecha;
        this.montomensual=montomensual;
    }
    //get
     public LocalDate getFecha() {
         return fecha;
     }
     public Double getMontomensual() {
        return montomensual;
    }
    
     //set
     public void setFecha(LocalDate fecha) {
         this.fecha = fecha;
     }
     public void setMontomensual(double montomensual) {
        this.montomensual = montomensual;
    }
   
    @Override
    public boolean equals(Object obj) {
        Lectura aComparar = (Lectura) obj;
        return this.getFecha().equals(aComparar.getFecha());
    }
}

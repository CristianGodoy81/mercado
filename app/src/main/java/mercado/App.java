package mercado;

import javax.swing.WindowConstants;
import mercado.Interfazgrafica.MenuPrincipalVentana;

public class App {
    private static Mercado mercadoDatos = Mercado.getInstance();
    
    public static void main(String[] args) {
        
        MenuPrincipalVentana ventana = new MenuPrincipalVentana(mercadoDatos);
          ventana.setVisible(true);
          ventana.setLocationRelativeTo(null);
          ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        

    }
}
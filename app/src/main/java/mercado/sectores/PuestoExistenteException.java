package mercado.sectores;

public class PuestoExistenteException extends RuntimeException{
    public PuestoExistenteException(){
        super("El puesto existe");
    }    
}

package mercado.sectores;

public class PuestoNoExistenteException extends RuntimeException{
    public PuestoNoExistenteException(){
        super("El puesto no existe");
    }    
}

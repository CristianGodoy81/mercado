package mercado.sectores;

import java.util.ArrayList;

public class Sector {
    
    // Atributos

    private String nombre;
    private String ubicacion;
    private ArrayList<Puesto> puestos; // Relacion: Un sector tiene muchos puestos.
    
    // Constructor

    public Sector(String nombre, String ubicacion){
        this.ubicacion = ubicacion;
        this.nombre = nombre;
        this.puestos = new ArrayList<Puesto>();
    }

    //gets
    public String getUbicacion() {
        return ubicacion;
    }

    // Metodos
    
    private boolean buscarPuesto(Puesto puesto){
        for(Puesto elemento : this.puestos){
            if(elemento.equals(puesto)){
                return true;
            }
        }
        return false;
    }

    public void agregarPuesto(Puesto puesto) throws PuestoExistenteException{
        if(buscarPuesto(puesto)){ // El puesto existe.
            throw new PuestoExistenteException();
        }else{ // El puesto no existe.
            this.puestos.add(puesto);
        }
    }

    public void quitarPuesto(Puesto puesto) throws PuestoNoExistenteException{
        if(buscarPuesto(puesto)){ // El puesto existe.
            this.puestos.remove(puesto);
        }else{ // El puesto no existe.
            throw new PuestoNoExistenteException();
        }
    }

    @Override
    public boolean equals(Object obj){
        Sector aComparar = (Sector) obj;
        return this.nombre.equals(aComparar.nombre);
    }

    // Getters

    public String getNombre(){
        return nombre;
    }

    public ArrayList<Puesto> getPuestos(){
        return puestos;
    }

    // Setters

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setPuestos(ArrayList<Puesto> puestos){
        this.puestos = puestos;
    }

}

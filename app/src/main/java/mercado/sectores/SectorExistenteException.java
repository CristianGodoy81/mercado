package mercado.sectores;

public class SectorExistenteException extends RuntimeException{
    public SectorExistenteException(){
        super("El sector existe");
    } 
}

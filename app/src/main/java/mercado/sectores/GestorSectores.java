package mercado.sectores;

import java.util.ArrayList;


public class GestorSectores {
    
    // Atributos

    private ArrayList<Sector> sectores;
    
    // Constructor

    public GestorSectores(){
        setSectores( new ArrayList<Sector>() );
    }

    // Metodos

    // sets
      public void setSectores(ArrayList<Sector> sectores) {
      this.sectores = sectores;
      }

      //gets
      public ArrayList<Sector> getSectores() {
          return sectores;
      }
   

    private boolean buscarSector(Sector sector){
        for(Sector elemento : this.sectores){
            if(elemento.equals(sector)){
                return true;
            }
        }
        return false;
    }

    public void agregarSector(Sector sector) throws SectorExistenteException{
        if(buscarSector(sector)){ // El sector existe.
            throw new SectorExistenteException();
        }else{ // El sector no existe.
            this.sectores.add(sector);
        }
    }

    public void quitarSector(Sector sector) throws SectorNoExistenteException{
        if(buscarSector(sector)){ // El sector existe.
            this.sectores.remove(sector);
        }else{ // El sector no existe.
            throw new SectorNoExistenteException();
        }
    }

    public void modificarSector( Sector sectorbuscar)throws SectorNoExistenteException{
        if(buscarSector(sectorbuscar)){
            this.sectores.remove(sectorbuscar);
            this.sectores.add(sectorbuscar);
        }else throw new SectorNoExistenteException();
    }

    //busco un sector por su nombre y devuelvo la información de ese sector, si no existe lanzo excepcion
    public Sector getSector(Sector sectorbuscar)throws SectorNoExistenteException{
        if(!buscarSector(sectorbuscar)){
            throw new SectorNoExistenteException();
        }

        Sector sectorbuscado = null;
        for(Sector sector : sectores){
            if(sector.equals(sectorbuscar)){

                sectorbuscado=sector;
            }

        }
        return sectorbuscado;
    }
}

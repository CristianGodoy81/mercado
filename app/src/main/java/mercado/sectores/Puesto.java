package mercado.sectores;

import mercado.Lectura.Medidor;

public class Puesto {
    
    // Atributos

    private Integer numeroPuesto;
    private boolean tieneTecho;
    private boolean tieneCamaraRefrigerante;
    private double dimensionesM2;
    private double precioMensual;
    private boolean estaDisponible;
    private Medidor medidor; // Relacion: Un puesto tiene un medidor.
    
    // Constructor

    public Puesto(Integer numeroPuesto, boolean tieneTecho, boolean tieneCamaraRefrigerante, double dimensionesM2, double precioMensual, boolean estaDisponible, Medidor medidor){
        this.numeroPuesto = numeroPuesto;
        this.tieneTecho = tieneTecho;
        this.tieneCamaraRefrigerante = tieneCamaraRefrigerante;
        this.dimensionesM2 = dimensionesM2;
        this.precioMensual = precioMensual; // Esto se debe eliminar (seguramente).
        this.estaDisponible = estaDisponible; // Esto se debe eliminar (posiblemente).
        this.medidor = medidor;
    }

    // Metodos

    @Override
    public boolean equals(Object obj){
        Puesto aComparar = (Puesto) obj;
        return this.numeroPuesto.equals(aComparar.numeroPuesto);
    }
    
    // Getters

    public Integer getNumeroPuesto(){
        return this.numeroPuesto;
    }

    public boolean getTieneTecho(){
        return this.tieneTecho;
    }
    
    public boolean getTieneCamaraRefrigerante(){
        return this.tieneCamaraRefrigerante;
    }
    
    public double getDimensionesM2(){
        return this.dimensionesM2;
    }
    
    public double getPrecioMensual(){
        return this.precioMensual;
    }
    
    public boolean getEstaDisponible(){
        return this.estaDisponible;
    }

    public Medidor getMedidor(){
        return this.medidor;
    }
    
    // Setters

    public void setNumeroPuesto(Integer numeroPuesto){
        this.numeroPuesto = numeroPuesto;
    }

    public void setTieneTecho(boolean tieneTecho){
        this.tieneTecho = tieneTecho;
    }
    
    public void setTieneCamaraRefrigerante(boolean tieneCamaraRefrigerante){
        this.tieneCamaraRefrigerante = tieneCamaraRefrigerante;
    }
    
    public void setDimensionesM2(double dimensionesM2){
        this.dimensionesM2 = dimensionesM2;
    }
    
    public void setPrecioMensual(double precioMensual){
        this.precioMensual = precioMensual;
    }
    
    public void setEstaDisponible(boolean estaDisponible){
        this.estaDisponible = estaDisponible;
    }

    public void setMedidor(Medidor medidor){
        this.medidor = medidor;
    }

}

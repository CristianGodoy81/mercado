package mercado.sectores;

public class SectorNoExistenteException extends RuntimeException{
    public SectorNoExistenteException(){
        super("El sector no existe");
    }
}

package mercado.clientes;

public class ClienteExistenteException extends RuntimeException{

    public ClienteExistenteException(){
        super("Cliente existente");
    }
    
}

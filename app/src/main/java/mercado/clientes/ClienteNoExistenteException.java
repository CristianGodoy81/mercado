package mercado.clientes;

public class ClienteNoExistenteException extends RuntimeException{

    public ClienteNoExistenteException(){
        super("Cliente no existe");
    }

}

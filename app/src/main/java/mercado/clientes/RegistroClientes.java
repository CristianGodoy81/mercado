package mercado.clientes;

import java.util.ArrayList;


public class RegistroClientes {

    // Atributos

    private ArrayList<Cliente> clientes;

    // Constructor

    public RegistroClientes(){
     setClientes(new ArrayList<Cliente>());
    }

    //set
    public void setClientes(ArrayList<Cliente> clientes) {
    this.clientes = clientes;
    }

    //get
    public ArrayList<Cliente> getClientes() {
        return clientes;
    }
 
    
    // Metodos

    private boolean existeCliente(Cliente cliente){
        for(Cliente elemento : this.clientes){
            if(elemento.equals(cliente)){
                return true;
            }
        }
        return false;
    }

    public void agregarCliente(Cliente cliente) throws ClienteExistenteException{
        if(existeCliente(cliente)){
            throw new ClienteExistenteException();
        }else{
            this.clientes.add(cliente);
        }
    }

    public void quitarCliente(Cliente cliente) throws ClienteNoExistenteException{
        if(existeCliente(cliente)){
            this.clientes.remove(cliente);
        }else{
            throw new ClienteNoExistenteException();
        }
    }

    
    public Cliente getCliente(Cliente clientebuscar)throws ClienteNoExistenteException{

        for( Cliente cliente : clientes){
            if(cliente.getCuit().equals(clientebuscar.getCuit())){
                return cliente;
        }

        }
      return null;
   }


   public Cliente buscarCliente(Cliente clientebuscar)throws ClienteNoExistenteException{
        Cliente buscarcliente = getCliente(clientebuscar);

        if(buscarcliente==null){
            throw new ClienteNoExistenteException();
        }
        return buscarcliente;
    }

    public void modificarCliente(Cliente clientemodificado)throws ClienteNoExistenteException{
        Cliente amodificar = getCliente(clientemodificado);

        if(amodificar==null){
            throw new ClienteNoExistenteException();
        }else{
            this.clientes.remove(amodificar);
            this.clientes.add(clientemodificado);
        }
    }

}
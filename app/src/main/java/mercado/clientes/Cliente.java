package mercado.clientes;

public abstract class Cliente {
    
    // Atributos
    
    private String cuit;
    private String telefono;
    private String mail;
    private String direccion;

    // Constructor
    
    public Cliente(String cuit, String telefono, String mail, String direccion){
        this.cuit = cuit;
        this.telefono = telefono;
        this.mail = mail;
        this.direccion = direccion;
    }

    // Metodos
    
    @Override
    public boolean equals(Object obj) {
        Cliente aComparar = (Cliente) obj;
        return this.getCuit().equals(aComparar.getCuit());
    }

    // Getters
    
    public String getCuit(){
        return this.cuit;
    }
    
    public String getTelefono(){
        return this.telefono;
    }
    
    public String getMail(){
        return this.mail;
    }
    
    public String getDireccion(){
        return this.direccion;
    }

    // Setters
    
    public void setCuit(String cuit){
        this.cuit = cuit;
    }
    
    public void setTelefono(String telefono){
        this.telefono = telefono;
    }
    
    public void setMail(String mail){
        this.mail = mail;
    }
    
    public void setDireccion(String direccion){
        this.direccion = direccion;
    }

}

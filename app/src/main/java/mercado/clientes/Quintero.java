package mercado.clientes;

public class Quintero extends Cliente{
    
    // Atributos

    private String nombre;
    private String apellido;

    // Constructor
    
    public Quintero(String cuit, String telefono, String mail, String direccion, String nombre, String apellido){
        super(cuit, telefono, mail, direccion);
        this.nombre = nombre;
        this.apellido = apellido;
    }

    // Metodos

    // Getters
    
    public String getNombre(){
        return this.nombre;
    }
    
    public String getApellido(){
        return this.apellido;
    }

    // Setters
    
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
    public void setApellido(String apellido){
        this.apellido = apellido;
    }

}

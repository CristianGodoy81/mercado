package mercado.clientes;

public class Empresa extends Cliente{
    
    // Atributos

    private String razonSocial;

    // Constructor

    public Empresa(String cuit, String telefono, String mail, String direccion, String razonSocial){
        super(cuit, telefono, mail, direccion);
        this.razonSocial = razonSocial;
    }

    // Metodos

    // Getters

    public String getRazonSocial(){
        return this.razonSocial;
    }

    // Setters

    public void setRazonSocial(String razonSocial){
        this.razonSocial = razonSocial;
    }

}
